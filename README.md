Example template:
- create git repository
- add labRemote as a submodule:
  `git submodule add https://gitlab.cern.ch/berkeleylab/labRemote.git`
- create/copy a CMakeLists.txt file
- inside `src` directory define your project (with corresponding CMakeLists.txt which will include all needed dependecies)
- commit and push changes to your git repository

To compile:
- `mkdir build`
- `cd build`
- `cmake ../`
- `make`

To update the labRemote submodule:
  `git submodule update`

To push the latest submodule version:
- `git commit -m "Pulled down update to submodule_dir"`
- `git push origin master`
